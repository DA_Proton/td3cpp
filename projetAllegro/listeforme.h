#pragma once
#include "forme.h"
#include <iostream>

using namespace std;

class listeforme
{
protected:
	list<forme*> l;
	list<forme*>::iterator it;
public:
	listeforme();
	~listeforme();
	void ajoute(forme*);
	void supprime(forme*);
	void affiche();
	forme* selecton(int, int);
};


#include "menuter.h"
#include <iostream>

using namespace std;

menuter::menuter(char* t[], int pn)
{
	n = pn;
	tab = new vector<bouton>(n);
	for (int i = 0; i < n; i++)
		tab->push_back(bouton(10 * i, 5 * i, t[i])); // on insere en fin de vecteur
}


menuter::~menuter()
{
	delete tab;
}

menuter::menuter(const menuter& s)
{
	n = s.n;
	tab = new vector<bouton>(n);
	for (int i = 0; i < n; i++)
		(*tab)[i] = (*s.tab)[i];
}

menuter& menuter::operator=(const menuter& s)
{
	if (this != &s)
	{
		delete tab;
		n = s.n;
		tab = new vector<bouton>(n);
		for (int i = 0; i < n; i++)
			(*tab)[i] = (*s.tab)[i];
	}
	return *this;
}

void menuter::affiche()
{
	cout << "menuter: " << endl;
	for (int i = 0; i < n; i++)
		(*tab)[i].affiche();
}

int menuter::selection(float px, float py)
{
	for (int i = 0; i < n; i++)
		if ((*tab)[i].selection(px, py) == true) return i;
	return -1;
}
#pragma once
#include "bouton.h"
class menubis
{
protected:
	bouton** tab;
	int n;
public:
	menubis(char*[],int);
	~menubis();
	menubis(const menubis&);
	menubis& operator=(const menubis&);
	void affiche();
	int selection(float px, float py);
};
#pragma once
#include "point.h"
#include "forme.h"
class rectangle :forme
{
protected:
	point p1, p2;
public:
	rectangle();
	rectangle(float, float, float, float);
	rectangle(point, float, float);
	rectangle(point, point);
	~rectangle();
	void init(float, float, float, float);
	void affiche();
	bool selection(float, float);
	rectangle(const rectangle&);
	rectangle& operator=(const rectangle&);
};


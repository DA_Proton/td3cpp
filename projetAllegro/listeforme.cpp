#include "listeforme.h"



listeforme::listeforme()
{
}


listeforme::~listeforme()
{
	for (it = l.begin(); it != l.end(); ++it) {
		l.remove(*it);
		delete *it;
	}
}

void listeforme::ajoute(forme* p)
{
	l.push_back(p);
}

void listeforme::supprime(forme* p)
{
	l.remove(p);
}

void listeforme::affiche()
{
	for (it = l.begin(); it != l.end(); it++)
	{
		(*it)->affiche();
		cout << "-----------------" << endl;
	}
	cout << "nbbr obj liste = " << l.size() << endl;
}

forme* listeforme::selecton(int sx, int sy)
{
	for (it = l.begin(); it != l.end(); it++)
		if ((*it)->selection(sx, sy)) return *it;
	return NULL;
}
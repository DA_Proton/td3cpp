#pragma once
class point
{
protected:
	float x, y, rho, theta;
	void majpol();
public:
	point();
	point(float);
	point(float, float);
	~point();
	void affiche();
	void init(float, float);
	void deplace(float, float);
	void homotetie(float);
	void rotation(float);
	int coincide(point);
	int getx();
	int gety();
};



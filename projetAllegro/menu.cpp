#include "menu.h"
#include <iostream>

using namespace std;

menu::menu(char* t[], int pn)
{
	n = pn;
	tab = new bouton[n];
	for (int i = 0; i < n; i++)
		tab[i].init(10 * i, 5 * i, t[i]);
}

menu::~menu()
{
	delete[]tab;
}

menu::menu(const menu& s)
{
	n = s.n;
	tab = new bouton[n];
	for (int i = 0; i < n; i++)
		tab[i] = s.tab[i];
}

menu& menu::operator=(const menu& s)
{
	if (this != &s)
	{
		delete[]tab;
		n = s.n;
		tab = new bouton[n];
		for (int i = 0; i < n; i++)
			tab[i] = s.tab[i];
	}
	return *this;
}

void menu::affiche()
{
	cout << "menu: " << endl;
	for (int i = 0; i < n; i++)
		tab[i].affiche();
}

int menu::selection(float px, float py)
/***********************************
* return -1 if no button selected  *
***********************************/
{
	for (int i = 0; i < n; i++)
		if (tab[i].selection(px,py) == true) return i;
	return -1;
}
#include "point.h"
#include <iostream>
#include <cmath>

using namespace std;

point::point()
{
	cout << "constructeur 1" << endl;
	x = 0; y = 0;
	majpol();
}

point::point(float px)
{
	cout << "constructeur 2" << endl;
	x = px;
	y = px;
	majpol();
}

point::point(float px, float py)
{
	cout << "constructeur 3" << endl;
	x = px;
	y = py;
	majpol();
}

point::~point()
{
	cout << "destructeur" << endl;
}

void point::affiche()
{
	cout << "x = " << x << endl //on affiche x = (valeur de x)
		<< "y = " << y << endl
		<< "rho = " << rho << endl
		<< "theta = " << theta << endl;
}

void point::init(float px, float py)
{
	x = px; y = py; majpol();
}

void point::deplace(float p1, float p2)
{
	x += p1; y += p2; majpol();
}

void point::homotetie(float p)
{
	x *= p; y *= p; majpol();
}

void point::majpol()
{
	rho = sqrt((pow(x, 2) + pow(y, 2)));
	theta = atan2(y, x);
}

void point::rotation(float alpha) //angle a entrer degree !!
{
	alpha = alpha * 3.14 / 180; //remplacer 3.14 par M_PI
	theta += alpha;
	x = rho * cos(theta);
	y = rho * sin(theta);
}

int point::coincide(point p2)
{
	if ((x == p2.x) && (y == p2.y)) return 1;
	return 0;
}

int point::getx()
{
	return x;
}

int point::gety()
{
	return y;
}
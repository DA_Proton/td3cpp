#pragma once
#include <vector>
#include "bouton.h"

using namespace std;

class menuter
{
protected:
	vector<bouton>* tab;
	int n;
public:
	menuter(char*[],int);
	~menuter();
	menuter(const menuter&);
	menuter& operator=(const menuter&);
	void affiche();
	int selection(float, float);
};


#pragma once
#include "rectangle.h"
#include "point.h"
#include <string>

using namespace std;

class bouton
{
protected:
	rectangle r;
	string ch;
public:
	bouton();
	bouton(float, float, char*);
	bouton(point, point, char*);
	bouton(point, float, float);
	bouton(point, float, float, char*);
	bouton(float, float, float, float, char*);
	bouton(rectangle, char*);
	~bouton();
	bouton(const bouton&); //pas oblig�
	bouton& operator=(const bouton&); //pas oblig�
	void init(float, float, char*);
	void affiche();
	bool selection(float sx, float sy) { return r.selection(sx, sy); }
};

